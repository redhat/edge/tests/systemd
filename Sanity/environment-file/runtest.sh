#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/environment-file
#   Description: Testing of environment file
#   Author: Branislav Blaskovic <bblaskov@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"
export PAGER=
CHINESSE=$RANDOM
SCRIPT_PATH=/usr/local/bin/environmentTest

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"

cat >$SCRIPT_PATH <<EOF
#!/bin/bash
echo "\$@"
echo "INSIDE SCRIPT: VARFOO=\${VARFOO} VARFOO2=\${VARFOO2} VARBAR=\${VARBAR} VARBAR2=\${VARBAR2}"
EOF
        rlRun "chmod +x $SCRIPT_PATH"
cat >/etc/environment-testing.conf <<EOF
VARBAR=ahojbar$CHINESSE
EOF
cat >/etc/environment-testing2.conf <<EOF
VARBAR2=ahojbar2$CHINESSE
EOF

cat >/etc/systemd/system/environment-testing.service <<EOF
[Unit]
Description=Environment testing service

[Service]
Type=simple
EnvironmentFile=/etc/environment-testing.conf
EnvironmentFile=/etc/environment-testing2.conf
Environment=VARFOO=ahoj$CHINESSE
Environment=VARFOO2=ahoj2$CHINESSE
ExecStart=$SCRIPT_PATH "START: VARFOO=\${VARFOO} VARFOO2=\${VARFOO2} VARBAR=\${VARBAR} VARBAR2=\${VARBAR2}"
ExecStartPre=$SCRIPT_PATH "POST START: VARFOO=\${VARFOO} VARFOO2=\${VARFOO2} VARBAR=\${VARBAR} VARBAR2=\${VARBAR2}"
ExecStartPre=$SCRIPT_PATH "PRE START: VARFOO=\${VARFOO} VARFOO2=\${VARFOO2} VARBAR=\${VARBAR} VARBAR2=\${VARBAR2}"

[Install]
WantedBy=multi-user.target
EOF
        rlRun "systemctl daemon-reload"
        rlRun "systemctl start environment-testing.service"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "journalctl -u environment-testing.service | tee journal.out"
        rlAssertGrep "START: VARFOO=ahoj$CHINESSE VARFOO2=ahoj2$CHINESSE VARBAR=ahojbar$CHINESSE VARBAR2=ahojbar2$CHINESSE" journal.out
        rlAssertGrep "PRE START: VARFOO=ahoj$CHINESSE VARFOO2=ahoj2$CHINESSE VARBAR=ahojbar$CHINESSE VARBAR2=ahojbar2$CHINESSE" journal.out
        rlAssertGrep "POST START: VARFOO=ahoj$CHINESSE VARFOO2=ahoj2$CHINESSE VARBAR=ahojbar$CHINESSE VARBAR2=ahojbar2$CHINESSE" journal.out
        rlAssertGrep "INSIDE SCRIPT: VARFOO=ahoj$CHINESSE VARFOO2=ahoj2$CHINESSE VARBAR=ahojbar$CHINESSE VARBAR2=ahojbar2$CHINESSE" journal.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm -rf /etc/environment-testing.conf /etc/environment-testing2.conf /etc/systemd/system/environment-testing.service $SCRIPT_PATH"
        rlRun "systemctl daemon-reload"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
