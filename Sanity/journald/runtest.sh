#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/journald
#   Description: Check journald functionality
#   Author: Frantisek Sumsal <fsumsal@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"
JOURNALD_CONFIG="/etc/systemd/journald.conf"
RSYSLOG_CONFIG="/etc/rsyslog.conf"
RSYSLOG_OVERRIDE="/etc/systemd/system/rsyslog.service"
RSYSLOG_SERVICE="/usr/lib/systemd/system/rsyslog.service"
SOCKET_WANTS_DIR="/etc/systemd/system/sockets.target.wants"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TestDir=$PWD"
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlFileBackup $RSYSLOG_CONFIG $JOURNALD_CONFIG
    rlPhaseEnd

    rlPhaseStartTest "[BZ#1409659] journal: forward messages from /dev/log unmodified to syslog.socket"
        # Create an override config file with with syslog alias
        # Since the [Install] directive cannot be overridden by a drop-in
        # file, we need to copy the original service file and change it directly
        rlRun "cp -fv $RSYSLOG_SERVICE $RSYSLOG_OVERRIDE"
        # Append 'Alias=syslog.service' directive after the [Install] section
        rlRun "sed -i'' '/\[Install\]/a Alias=syslog.service' $RSYSLOG_OVERRIDE"
        rlRun "systemctl stop rsyslog.service"
        rlRun "systemctl -q is-enabled rsyslog && systemctl disable rsyslog"
        rlRun "systemctl enable rsyslog.service"

        rlRun "cp -fv $TestDir/rsyslog.conf /etc/rsyslog.conf"
        # Configure journald to forward logs to syslog socket
        rlRun "echo 'ForwardToSyslog=yes' >> $JOURNALD_CONFIG"
        # Make sure socket.target.wants dir exists
        rlRun "mkdir -p $SOCKET_WANTS_DIR"
        rlRun "[ -d $SOCKET_WANTS_DIR ]"
        # Statically enable syslog.socket
        rlRun "ln -s /usr/lib/systemd/system/syslog.socket $SOCKET_WANTS_DIR/syslog.socket"

        # Apply configuration
        rlRun "systemctl daemon-reload"
        rlRun "systemctl restart systemd-journald"
        rlRun "systemctl cat rsyslog.service"
        rlRun "cat $RSYSLOG_CONFIG"
        rlRun "systemctl start syslog.socket"
        rlRun "systemctl start rsyslog.service"
        rlRun "systemctl status syslog.socket"
        rlRun "systemctl status rsyslog.service"

        # Random message with 5 whitespaces
        MESSAGE="This is a test message ($RANDOM $SECONDS)     "
        MESSAGE_TAG="whitespace$RANDOM"
        LOG="$(mktemp)"

        echo -ne "$MESSAGE" | logger -t $MESSAGE_TAG
        journalctl --sync
        # Verify, if we successfully sent the message
        # (this one can be, and probably will be, stripped of trailing whitespaces
        rlRun "journalctl -q -t $MESSAGE_TAG | tee $LOG"
        rlRun "[[ $(wc -l < $LOG) == 1 ]]"
        # Verify that /var/log/messages contains an unmodified message
        rlRun "grep '$MESSAGE' /var/log/messages"

        rlRun "rm -fv $LOG"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlFileRestore
        rlRun "rm $RSYSLOG_OVERRIDE $SOCKET_WANTS_DIR/syslog.socket"
        rlRun "systemctl daemon-reload"
        rlRun "systemctl disable rsyslog.service && systemctl enable rsyslog.service"
        rlRun "systemctl restart rsyslog.service systemd-journald.service"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
