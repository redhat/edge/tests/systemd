#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/systemd/Sanity/udev-smoke
#   Description: Test for basic functionality of udev
#   Author: Radka Skvarilova <rskvaril@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="systemd"

WRONG_RULE='XXX=  , ACTION=="add"'
RULEFILE1="/etc/udev/rules.d/smoke1.rules"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        STARTDATE=`date +'%Y-%m-%d %H:%M:%S'`
        # Skip LVM DM devices
        DEVICE="$(blkid | awk -F ':' '! /\/dev\/mapper\// { printf $1; exit }')"
        DEVICE_NAME="${DEVICE##*/}"

        rlLogInfo "DEVICE: $DEVICE"
        rlLogInfo "DEVICE_NAME: $DEVICE_NAME"
        if [[ -z "$DEVICE" || -z "$DEVICE_NAME" ]]; then
            rlDie "Failed to find & parse a suitable device"
        fi
    rlPhaseEnd
 #---------------test 1----------------------
    rlPhaseStartTest "TEST1 Setting wrong rule"
        rlRun "echo '$WRONG_RULE' > $RULEFILE1" 0 "Writing the testing rule"
        rlRun "udevadm control --reload && udevadm trigger"
        rlRun "journalctl --sync"
        logger test1 #just for debugging
        rlRun "journalctl --unit systemd-udevd --since '$STARTDATE' | tee test1.log"
        rlLogInfo "Checking that udev reported the murder of the stale process"
        if rlIsRHEL "<=8"; then
            rlAssertGrep "invalid key/value pair in file /etc/udev/rules.d/smoke1.rules" test1.log
        else
            rlAssertGrep "/etc/udev/rules.d/smoke1.rules:1 Invalid key/value pair" test1.log
        fi
        rlLog "debug"
        cat test1.log
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm -rf $RULEFILE1"
        rlRun "udevadm control --reload && udevadm trigger"
     rlPhaseEnd

 #---------------test 2----------------------
    rlPhaseStartTest "TEST2 udevadm info testing"
        #name
        rlLog "test --name "
        rlRun  "udevadm info $DEVICE |tee test2.log"
        rlAssertGrep "DEVNAME=$DEVICE" test2.log
        rlRun "udevadm info --name=$DEVICE_NAME |tee test3.log"
        rlAssertGrep "DEVNAME=$DEVICE" test3.log
        if rlIsFedora || rlIsRHEL ">=8" || rlIsCentOS ">=8"; then
            rlLog "Sort symlinks - only on Fedora/RHEL8+"
            #Fedora/RHEL8 display the links in different order (it is not a bug, but we need to sort it for test)
            #this code sort symlinks
            Devlinks2=$(grep DEVLINKS test2.log | cut -d "=" -f 2 | tr " " "\n" | sort | tr "\n" " ")
            Devlinks3=$(grep DEVLINKS test3.log | cut -d "=" -f 2 | tr " " "\n" | sort | tr "\n" " ")
            sed 's|^.*DEVLINKS.*$|E: DEVLINKS='"${Devlinks2}"'|' test2.log | tee xtest2.log
            sed 's|^.*DEVLINKS.*$|E: DEVLINKS='"${Devlinks3}"'|' test3.log | tee xtest3.log
            # Drop TAGS= and CURRENT_TAGS= properties, since they are in an arbitrary order,
            # which breaks the output comparing below
            sed -ri '/^E: (CURRENT_)?TAGS=/d' xtest2.log
            sed -ri '/^E: (CURRENT_)?TAGS=/d' xtest3.log
            rlRun "diff <(sort xtest2.log) <(sort xtest3.log)" 0 "Check if there is no difference between previous output"
        else
            rlRun "diff <(sort test2.log) <(sort test3.log)" 0 "Check if there is no difference between previous output"
        fi
        #query name
        rlLog "test --query name"
        rlRun -s "udevadm info --query=name --name=$DEVICE_NAME"
        rlAssertGrep "$DEVICE_NAME" $rlRun_LOG
        #query name root
        rlLog "test --query name with --root"
        rlRun -s "udevadm info --query=name --name=$DEVICE_NAME --root"
        rlAssertGrep "/dev/$DEVICE_NAME" $rlRun_LOG
        #query symlink
        rlLog "test --query symlink"
        rlRun -s "udevadm info --query=symlink --name=$DEVICE_NAME"
        rlLog "Check if the output is without '/dev/'"
        rlAssertNotGrep " /dev/" $rlRun_LOG #chceck if there is not /dev preposition
        #query symlink root
        rlLog "test --query symlink with --root"
        rlRun -s "udevadm info --query=symlink --name=$DEVICE_NAME --root"
        for j in $rlRun_LOG ; do
            ls -l $i |grep $DEVICE_NAME
        done
        #query path
        rlLog "test --query path"
        rlRun -s "udevadm info --query=path --name=$DEVICE_NAME"
        DevPath=$(udevadm info $DEVICE|grep DEVPATH= |cut -d "=" -f2)
        rlAssertGrep "$DevPath" $rlRun_LOG
        #query all
        rlLog "test --query all"
        rlRun -s "udevadm info --query=all --name=$DEVICE_NAME"
        #check output TODO

    rlPhaseEnd

    rlPhaseStartCleanup
        #just for sure
        rlRun "udevadm control --reload-rules && udevadm trigger"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
